import 'package:flutter/material.dart';

final Map<int, Color> colorCodes = {
  50: Color.fromRGBO(26, 143, 122, 1),
  100: Color.fromRGBO(26, 143, 122, 1),
  200: Color.fromRGBO(26, 143, 122, 1),
  300: Color.fromRGBO(26, 143, 122, 1),
  400: Color.fromRGBO(26, 143, 122, 1),
  500: Color.fromRGBO(26, 143, 122, 1),
  600: Color.fromRGBO(26, 143, 122, 1),
  700: Color.fromRGBO(239, 222, 225, 1),
  800: Color.fromRGBO(239, 222, 234, 1),
  900: Color.fromRGBO(237, 237, 237, 1),
};

final theme = ThemeData(
  primarySwatch: MaterialColor(0xFF1A8F7A, colorCodes),
  visualDensity: VisualDensity.adaptivePlatformDensity,
  fontFamily: 'Raleway',
  textTheme: TextTheme(
    bodyText1: TextStyle(color: colorCodes[50]),
    bodyText2: TextStyle(color: colorCodes[50]),
  ),
  cardTheme: CardTheme(
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(15)))),
  bottomAppBarTheme: BottomAppBarTheme(elevation: 30, color: Colors.white),
);
