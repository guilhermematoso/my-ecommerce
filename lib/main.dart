import 'package:ecommerce/providers/providers.dart';
import 'package:ecommerce/routes.dart';
import 'package:ecommerce/utils/massa_de_dados.dart';
import 'package:ecommerce/utils/theme.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  await MassaDeDados.inserirProdutos();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: providers,
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        theme: theme,
        initialRoute: "/home",
        routes: routes,
      ),
    );
  }
}
