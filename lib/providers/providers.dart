import 'package:ecommerce/providers/cart_provider.dart';
import 'package:ecommerce/providers/product_provider.dart';
import 'package:ecommerce/providers/user_provider.dart';
import 'package:provider/provider.dart';
import 'package:provider/single_child_widget.dart';

final List<SingleChildWidget> providers = [
  ChangeNotifierProvider(
    create: (_) => new UserProvider(),
    lazy: false,
  ),
  ChangeNotifierProvider(
    create: (_) => new ProductProvider(),
    lazy: false,
  ),
  ChangeNotifierProxyProvider<UserProvider, CartProvider>(
    create: (_) => new CartProvider(),
    update: (_, _userProvider, _cartProvider) =>
        _cartProvider..updateUser(_userProvider),
    lazy: false,
  )
];
