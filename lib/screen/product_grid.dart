import 'package:ecommerce/models/product_model.dart';
import 'package:ecommerce/providers/product_provider.dart';
import 'package:ecommerce/widgets/product/product_grid_item.dart';
import 'package:ecommerce/widgets/product/search_field.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:provider/provider.dart';

class ProductGrid extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: Color.fromRGBO(26, 143, 122, 1),
        elevation: 0,
        toolbarHeight: 70,
        bottom: PreferredSize(
          preferredSize: const Size.fromHeight(70),
          child: Padding(
            padding: const EdgeInsets.only(bottom: 10),
            child: Container(
              padding: const EdgeInsets.symmetric(horizontal: 20),
              height: 50,
              alignment: Alignment.center,
              child: Row(
                children: [
                  Expanded(
                    flex: 7,
                    child: SearchField(),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
      body: Container(
        padding: const EdgeInsets.symmetric(vertical: 0, horizontal: 15),
        child: ListView(
          children: [
            const SizedBox(
              height: 20,
            ),
            Card(
              child: Image.asset(
                "assets/images/banner_01.png",
                fit: BoxFit.fill,
              ),
            ),
            const SizedBox(
              height: 40,
            ),
            const Text(
              "Destaques",
              style: TextStyle(fontSize: 20)
            ),
            Consumer<ProductProvider>(
              builder: (_, productProvider, child) {
                List<ProductModel> products = productProvider.products;

                return GridView.builder(
                  shrinkWrap: true,
                  physics: NeverScrollableScrollPhysics(),
                  itemCount: productProvider.count,
                  gridDelegate: SliverGridDelegateWithMaxCrossAxisExtent(
                    maxCrossAxisExtent: 250,
                    mainAxisSpacing: 8,
                    crossAxisSpacing: 8,
                    childAspectRatio: 250 / 330,
                  ),
                  itemBuilder: (context, index) {
                    return ProductGridItem(products[index]);
                  },
                );
              },
            )
          ],
        ),
      ),
    );
  }
}
