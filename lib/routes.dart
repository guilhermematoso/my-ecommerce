import 'package:ecommerce/screen/address_screen.dart';
import 'package:ecommerce/screen/cart_screen.dart';
import 'package:ecommerce/screen/home_screen.dart';
import 'package:ecommerce/screen/login_screen.dart';
import 'package:ecommerce/screen/product_details.dart';
import 'package:ecommerce/screen/product_grid.dart';
import 'package:ecommerce/screen/register_screen.dart';
import 'package:ecommerce/widgets/user/user_page_view.dart';
import 'package:flutter/cupertino.dart';

final Map<String, WidgetBuilder> routes = {
  "/home": (_) => HomeScreen(),
  "/login": (_) => LoginScreen(),
  "/register": (_) => RegisterScreen(),
  "/products": (_) => ProductGrid(),
  "/products/details": (_) => ProductDetailScreen(),
  "/cart": (_) => CartScreen(),
  "/address": (_) => AddressScreen(),
  "/me": (_) => UserPageView()
};
